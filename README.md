```console
mkvirtualenv --python=$(which python3) ml
```

```console
brew install gfortran

pip3 install numpy
pip3 install scipy
pip3 install matplotlib
pip3 install ipython
pip3 install ipykernel # for jupyter 
```

```console
git config --add merge.ff false
git remote add origin git@bitbucket.org:cliebmann/nearest-neighbors.git
git push -u origin --all
```

####From ~/.bashrc:
```shell
function frameworkpython3
{
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
        PYTHONHOME=$VIRTUAL_ENV /usr/local/bin/python3 "$@"
    else
        /usr/local/bin/python3 "$@"
    fi
}
```

```python
$ ipython3 notebook
In [1]: %matplotlib inline
```
