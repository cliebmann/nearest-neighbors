"""
Discover the periods in data/populations.txt
"""
import numpy as np
from scipy import fftpack
import matplotlib.pyplot as plt

data = np.loadtxt('data/populations.txt')
years = data[:, 0]
populations = data[:, 1:]

#ft_populations = np.fft.fft(populations, axis=0)
#frequencies = np.fft.fftfreq(populations.shape[0], years[1] - years[0])
ft_populations = fftpack.fft(populations, axis=0)
frequencies = fftpack.fftfreq(populations.shape[0], years[1] - years[0])
periods = 1 / frequencies
# scipy_periodicityfinder.py:18: RuntimeWarning: divide by zero encountered in true_divide

plt.figure()
plt.plot(years, populations * 1e-3)
plt.xlabel('Year')
plt.ylabel('Population number ($\cdot10^3$)')
plt.legend(['hare', 'lynx', 'carrot'], loc=1)

plt.figure()
plt.plot(periods, abs(ft_populations) * 1e-3, 'o')
plt.xlim(0, 22)
plt.xlabel('Period')
plt.ylabel('Power ($\cdot10^3$)')

plt.show()
