__all__ = ["student", "my_class"]

from student.student import Student
from student.student import MasterStudent
from student.my_class import MyClass
