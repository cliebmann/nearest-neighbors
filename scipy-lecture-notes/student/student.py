class Student(object):
    def __init__(self, name):
        self.name = name
    def set_age(self, age):
        self.age = age
    def set_major(self, major):
        self.major = major

class MasterStudent(Student):
    internship = 'mandatory, from March to June'

