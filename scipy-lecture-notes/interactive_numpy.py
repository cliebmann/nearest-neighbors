import numpy as np
a = np.array([0, 1, 2, 3])
a
import timeit
u = timeit.Timer("[i**2 for i in range(1000)")
u = timeit.Timer("[i**2 for i in range(1000)]")
print(u)
print(u.timeit())
u = timeit.Timer("np.arange(1000)**2", setup="import numpy as np")
print(u.timeit())
np.lookfor('create array')
a.ndim()
a.ndim
a.shape
len(a)
b = np.array([[0, 1, 2], [3, 4, 5]])
b
b.ndim
b.shape
len(b)
c = np.array([[[1], [2]], [[3], [4]]])
c
c.shape
a = np.arange(10)
a
b = np.arange(1, 9, 2) # start, end (exclusive), step
b
c = np.linspace(0, 1, 6)   # start, end, num-points
c
d = np.linspace(0, 1, 5, endpoint=False)
d
a = np.ones((3, 3))
a
b = np.zeros((2, 2))
c np.eye(3)
c = np.eye(3)
c
d = np.diag(np.array([1, 2, 3, 4]))
d
a = np.random.rand(4)       # uniform in [0, 1]
a
b = np.random.randn(4)      # Gaussian
b
b = np.random.randn(4)      # Gaussian
b
np.random.seed(1234)        # Setting the random seed
a = np.array([1, 2, 3])
a
a.dtype
b = np.array([1., 2., 3.])
b.type
b.dtype
c = np.array([1, 2, 3], dtype=float)
c.dtype
 a = np.ones((3, 3))
 a = np.ones((3, 3))
 a.dtype
 d = np.array([1+2j, 3+4j, 5+6*1j])
 d.dtype
 d
 e = np.array([True, False, False, True])
 e.dtype
 f = np.array(['Bonjour', 'Hello', 'Hallo',])
 f.dtype
 f
 import matplotlib.pyplot as plt  # the tidy way
"""
/Users/craig/.virtualenvs/ml/lib/python3.3/site-packages/matplotlib/font_manager.py:273: UserWarning: Matplotlib is building the font cache using fc-list. This may take a moment.
  warnings.warn('Matplotlib is building the font cache using fc-list. This may take a moment.')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/Users/craig/.virtualenvs/ml/lib/python3.3/site-packages/matplotlib/pyplot.py", line 114, in <module>
    _backend_mod, new_figure_manager, draw_if_interactive, _show = pylab_setup()
  File "/Users/craig/.virtualenvs/ml/lib/python3.3/site-packages/matplotlib/backends/__init__.py", line 32, in pylab_setup
    globals(),locals(),[backend_name],0)
  File "/Users/craig/.virtualenvs/ml/lib/python3.3/site-packages/matplotlib/backends/backend_macosx.py", line 24, in <module>
    from matplotlib.backends import _macosx
RuntimeError: Python is not installed as a framework. The Mac OS X backend will not be able to function correctly if Python is not installed as a framework. See the Python documentation for more information on installing Python as a framework on Mac OS X. Please either reinstall Python as a framework, or try one of the other backends. If you are Working with Matplotlib in a virtual enviroment see 'Working with Matplotlib in Virtual environments' in the Matplotlib FAQ



http://matplotlib.org/faq/usage_faq.html#what-is-a-backend
In ~/.bashrc:
function frameworkpython3
{
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
        PYTHONHOME=$VIRTUAL_ENV /usr/local/bin/python3 "$@"
    else
        /usr/local/bin/python3 "$@"
    fi
}

"""

