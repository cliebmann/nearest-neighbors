import module_demo

module_demo.print_a()
module_demo.print_b()


from student import Student, MasterStudent, MyClass

anna = Student('anna')
anna.set_age(21)
anna.set_major('physics')

james = MasterStudent('james')
james.internship
james.set_age(23)
james.age

the_class = MyClass()
the_class.add(anna)
the_class.add_student(anna)
the_class.add_student(james)
