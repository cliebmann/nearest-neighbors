import numpy as np
import pylab as plt
from scipy import misc

face = misc.face(gray=True)
plt.imshow(face, cmap=plt.cm.gray)
plt.show()

crop_face = face[100:-100, 100:-100]
sy, sx = face.shape
y, x = np.ogrid[0:sy, 0:sx] # x and y indices of pixels
centerx, centery = (660, 300) # center of the image
mask = ((y - centery)**2 + (x - centerx)**2) > 230**2 # circle
face[mask] = 0
plt.imshow(face)
plt.show()
