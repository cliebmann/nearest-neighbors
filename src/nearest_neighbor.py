import numpy as np

from utils import euclidean_distance, error_rate

class NearestNeighbor(object):
    def __init__(self, training_data):
        self.training_features = training_data['feature_vector']
        self.training_labels = training_data['labels']

    def nn(self, x):
        dists = [euclidean_distance(n, x) for n in self.training_features]
        min_dist_idx = np.argmin(dists)
        return self.training_labels[min_dist_idx]

class TestNearestNeighbor(object):
    def __init__(self, NN, tests, num_tests=None):
        if num_tests is None:
            num_tests = np.shape(tests['feature_vector'])[0]

        self.NN = NN
        self.test_examples = tests['feature_vector'][:num_tests]
        self.test_labels = tests['labels'][:num_tests]

    def compute_error_rate(self):
        my_labels = [self.NN.nn(te) for te in self.test_examples]
        return error_rate(my_labels, self.test_labels)