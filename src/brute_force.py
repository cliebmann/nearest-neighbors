import numpy as np
from scipy import io as spio
from timeit import default_timer as timer

from read_ocr_data import read_ocr_data, sample_training_data
from nearest_neighbor import NearestNeighbor, TestNearestNeighbor
from utils import error_rate

N = np.array([1000, 2000, 4000, 8000])
ITERATIONS = 10

def test_many():
    data = read_ocr_data()

    training_data = sample_training_data(data['training'], 1000)
    test_data = data['test']

    NN = NearestNeighbor(training_data)
    TNN = TestNearestNeighbor(NN, test_data, 1000)

    er = TNN.compute_error_rate()
    print(er)

def test_single():
    data = read_ocr_data()

    training_data = sample_training_data(data['training'], 1000)

    test_idx = 17
    test_example = data['test']['feature_vector'][test_idx]
    test_label = data['test']['labels'][test_idx]

    NN = NearestNeighbor(training_data)
    label = NN.nn(test_example)
    print(label, test_label)


def test_nearest_neighbor(training_data, test_data):
    error_rates = []
    for i in N:
        print("\tsubstep:", i)
        begin = timer()

        training_data = sample_training_data(training_data, i)
        NN = NearestNeighbor(training_data)
        TNN = TestNearestNeighbor(NN, test_data)

        er = TNN.compute_error_rate()
        end = timer()
        print("\t\terror_rate:", er, ", time:", end - begin)
        error_rates.append(er)

    return error_rates

def run_test_nearest_neighbor():
    print("read ocr data")
    ocr = read_ocr_data()
    error_rates = []
    for i in range(ITERATIONS):
        print("step:", i)
        error_rates.append(test_nearest_neighbor(ocr['training'], ocr['test']))

    return np.array(error_rates)

if __name__ == '__main__':
    error_rates = run_test_nearest_neighbor()
    spio.savemat('run_10_test_nearest_neighbor.mat',
                  mdict={'error_rates': error_rates})

    