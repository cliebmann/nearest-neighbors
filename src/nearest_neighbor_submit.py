import numpy as np
from scipy import io as spio

N = np.array([1000, 2000, 4000, 8000])
ITERATIONS = 10
OCR_IMAGE_DATA_FNAME = "../data/ocr.mat"


# read ocr data

def read_ocr_data():
    ocr = spio.loadmat(OCR_IMAGE_DATA_FNAME)
    return {
      'training': {
        'feature_vector': ocr['data'].astype(float),
        'labels': ocr['labels'],
      },
      'test': {
        'feature_vector': ocr['testdata'].astype(float),
        'labels': ocr['testlabels'],
      },
    }

def sample_training_data(training_data, n):
    num_samples, num_features = np.shape(training_data['feature_vector'])
    sample_group_idx = np.random.random_integers(1, num_samples, n) - 1
    return {
      'feature_vector': training_data['feature_vector'][sample_group_idx],
      'labels': training_data['labels'][sample_group_idx],
    }


# utils

def euclidean_distance(a, b):
    return np.sqrt(np.sum((a - b) ** 2))

def error_rate(a, b):
    num_examples = np.shape(a)[0]
    return np.sum(a != b, axis=0)[0] / num_examples


# nearest neighbor

class NearestNeighbor(object):
    def __init__(self, training_data):
        self.training_features = training_data['feature_vector']
        self.training_labels = training_data['labels']

    def nn(self, x):
        dists = [euclidean_distance(n, x) for n in self.training_features]
        min_dist_idx = np.argmin(dists)
        return self.training_labels[min_dist_idx]

class TestNearestNeighbor(object):
    def __init__(self, NN, tests, num_tests=None):
        if num_tests is None:
            num_tests = np.shape(tests['feature_vector'])[0]

        self.NN = NN
        self.test_examples = tests['feature_vector'][:num_tests]
        self.test_labels = tests['labels'][:num_tests]

    def compute_error_rate(self):
        my_labels = [self.NN.nn(te) for te in self.test_examples]
        return error_rate(my_labels, self.test_labels)

# drivers

def test_nearest_neighbor(training_data, test_data):
    error_rates = []
    for i in N:
        training_data = sample_training_data(training_data, i)
        NN = NearestNeighbor(training_data)
        TNN = TestNearestNeighbor(NN, test_data)

        er = TNN.compute_error_rate()
        error_rates.append(er)

    return error_rates

def run_test_nearest_neighbor():
    ocr = read_ocr_data()
    error_rates = []
    for i in range(ITERATIONS):
        error_rates.append(test_nearest_neighbor(ocr['training'], ocr['test']))

    return np.array(error_rates)

if __name__ == '__main__':
    error_rates = run_test_nearest_neighbor()
    spio.savemat('../data/run_10_test_nearest_neighbor.mat',
                  mdict={'error_rates': error_rates})