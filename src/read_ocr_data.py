import numpy as np
from scipy import io as spio

OCR_IMAGE_DATA_FNAME = "../data/ocr.mat"

def read_ocr_data():
    ocr = spio.loadmat(OCR_IMAGE_DATA_FNAME)
    return {
      'training': {
        'feature_vector': ocr['data'].astype(float),
        'labels': ocr['labels'],
      },
      'test': {
        'feature_vector': ocr['testdata'].astype(float),
        'labels': ocr['testlabels'],
      },
    }

def sample_training_data(training_data, n):
    num_samples, num_features = np.shape(training_data['feature_vector'])
    sample_group_idx = np.random.random_integers(1, num_samples, n) - 1
    return {
      'feature_vector': training_data['feature_vector'][sample_group_idx],
      'labels': training_data['labels'][sample_group_idx],
    }