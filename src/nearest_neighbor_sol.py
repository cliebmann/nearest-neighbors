import random
import numpy as np
from scipy import io as spio
from matplotlib import pyplot as plt

OCR_IMAGE_DATA_FNAME = "../data/ocr.mat"
SIZES = [1000, 2000, 4000, 8000]
REPEAT = 10

def one_nearest_neighbor(train_data, train_labels, test_data):
    # ||x_i||^2
    norms = np.square(np.linalg.norm(train_data, axis=1))
    # <x_i, t_j>
    cross = test_data.dot(train_data.T)
    # compute square distances (add norms to every row), ignore ||t_j||^2
    distances = -2 * cross + norms
    # for each row j, find (column) closest trainig data point
    indices = np.argmin(distances, axis=1)
    # grab label of closest training data point
    preds = train_labels[indices]
    return preds

def plot_results(results):
    means, stds = zip(*results)
    plt.figure()
    plt.errorbar(SIZES, means, yerr=stds)
    plt.title('Learning Curve')
    plt.xlabel('N')
    plt.ylabel('Error Rate')
    plt.show()

def main():
    # load data
    ocr = spio.loadmat(OCR_IMAGE_DATA_FNAME)

    # grab the test data and labels
    test_data = np.array(ocr['testdata']).astype('float')
    test_labels = np.array(ocr['testlabels'])

    # run the train/test
    results = []
    for n in SIZES:
        errors = np.zeros(REPEAT)
        for i in range(REPEAT):
            print("({0:d}, {1:d})".format(n, i))
            # draw random sample from training data
            sel = random.sample(range(len(ocr['data'])), n)
            train_data = np.array(ocr['data'][sel]).astype('float')
            train_labels = np.array(ocr['labels'][sel])
            # get the nearest neighbor predictions
            preds = one_nearest_neighbor(train_data, train_labels, test_data)
            # compute error rate
            errors[i] = np.count_nonzero(preds - test_labels) / float(len(test_labels))
        # save mean and standard deviation
        results.append((np.mean(errors), np.std(errors)))

    plot_results(results)

if __name__ == '__main__':
    main()
