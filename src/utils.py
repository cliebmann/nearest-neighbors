import numpy as np

def euclidean_distance(a, b):
    return np.sqrt(np.sum((a - b) ** 2))

def error_rate(a, b):
    num_examples = np.shape(a)[0]
    return np.sum(a != b, axis=0)[0] / num_examples