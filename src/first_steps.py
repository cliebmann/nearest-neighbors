# #!/usr/local/bin/python3
# frameworkpython3

import numpy as np
from scipy import io as spio
from timeit import default_timer as timer

OCR_IMAGE_DATA_FNAME = "../data/ocr.mat"

#N = np.array([1000, 2000, 4000, 8000])
N = np.array([1000])

def read_ocr_data():
    ocr = spio.loadmat(OCR_IMAGE_DATA_FNAME)
    return {
      'training': {
        'feature_vector': ocr['data'].astype(float),
        'labels': ocr['labels'],
      },
      'test': {
        'feature_vector': ocr['testdata'].astype(float)[:1000],
        'labels': ocr['testlabels'][:1000],
      },
    }

def sample_training_data(training_data, n):
    num_samples, num_features = np.shape(training_data['feature_vector'])
    sample_group_idx = np.random.random_integers(0, num_samples, n)
    return {
      'feature_vector': training_data['feature_vector'][sample_group_idx],
      'labels': training_data['labels'][sample_group_idx],
    }

def euclidean_distance(a, b):
    return np.sqrt(np.sum((a - b) ** 2, axis=0))

v_euclidean_distance = \
    np.vectorize(euclidean_distance,
                 otypes=[np.float],
                 excluded=['b'])

def nearest_neighbor(neighbors):
    def nearest_neighbor2(x):
        dists = euclidean_distance(a=neighbors['feature_vector'], b=x)
        min_dist_idx = np.argmin(dists)
        return neighbors['labels'][min_dist_idx]
    return nearest_neighbor2

v_nearest_neighbor = \
    np.vectorize(nearest_neighbor,
                 otypes=[np.uint8],
                 excluded=['neighbors'])

def test_nearest_neighbor(neighbors, tests):
    return np.apply_along_axis(
              func1d=nearest_neighbor(neighbors),
              axis=1,
              arr=tests)

def test_nearest_neighbor_iter(neighbors, tests):
    results = []
    my_nn = nearest_neighbor(neighbors)
    for t in tests:
        res = my_nn(t)
        results.append(res)
    return np.array(results)

def error_rate(a, b):
    num_examples = np.shape(a)[0]
    return np.sum(a != b, axis=0)[0] / num_examples

def run_test_nearest_neighbor(ocr_data):
    error_rates = []
    for i in N:
        print("\tsubstep: {:d}".format(i))
        start = timer()

        training_data = sample_training_data(ocr_data['training'], i)
        test_examples = ocr_data['test']['feature_vector']
        test_labels = ocr_data['test']['labels']

        print("\ttraining_data['feature_vector'].shape:", training_data['feature_vector'].shape)
        print("\ttraining_data['labels'].shape:", training_data['labels'].shape)
        print("\ttest_examples.shape:", test_examples.shape)
        print("\ttest_labels.shape:", test_labels.shape)

        labels = test_nearest_neighbor_iter(training_data, test_examples)

        print("\tlabels.shape:", labels.shape)

        er = error_rate(labels, test_labels)
        print("\terror rate:", er)
        
        end = timer()
        print("\ttest time:", end - start)
        print("\n")

        error_rates.append(er)

    return np.array(error_rates)

def run_n_test_nearest_neighbor(n):
    ocr_data = read_ocr_data()
    all_error_rates = []
    for i in range(n):
        print("step: {:d}".format(i))
        all_error_rates.append(run_test_nearest_neighbor(ocr_data))

    return np.array(all_error_rates)

def test_a():
    ocr_data = read_ocr_data()
    td = sample_training_data(ocr_data['training'], 20)
    test_examples = ocr_data['test']['feature_vector'][0:2]
    test_labels = ocr_data['test']['labels'][0:2]

    my_nearest_neighbor = nearest_neighbor(td)

    labels = my_nearest_neighbor(test_examples[0])
    print(labels, test_labels[0])

if __name__ == '__main__':
    all_error_rates = run_n_test_nearest_neighbor(1)
    #spio.savemat('run_10_test_nearest_neighbor.mat',
    #             mdict={'error_rates': all_error_rates})


