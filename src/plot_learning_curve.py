import numpy as np
from scipy import io as spio
from matplotlib import pyplot as plt

N = np.array([1000, 2000, 4000, 8000])

def learning_curve(error_rates):
    avgs = np.mean(error_rates, axis=0)
    stds = np.std(error_rates, axis=0)

    plt.title('Nearest Neighbor Learning Curve')
    plt.xlabel('training sample size')
    plt.ylabel('error rate')
    plt.scatter(N, avgs, 10, color='blue')
    plt.plot(N, avgs, color='blue')
    plt.errorbar(N, avgs, yerr=stds)
    plt.show()


if __name__ == '__main__':
    data = spio.loadmat('../data/run_10_test_nearest_neighbor.mat')
    error_rates = data['error_rates']
    learning_curve(error_rates)